<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h2 style="text-align: center;">BPKB Kendaraan</h2>
                    <h2 style="text-align: center; color: red">* mandatory(Harus di lengkapi)</h2>

                    <form action="" method="POST" enctype="multipart/form-data">
                        
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="kondisi_bpkp" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            Cek Kondisi fisik BPKB<span style="color:hsla(0,100%,50%,1);">*</span>
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="sinar_uv" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            Melakukan Senter Sinar UV pada BPKB(Watermark dan benang Pengaman)<span style="color:hsla(0,100%,50%,1);">*</span>
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="stnk" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                              Mengecek Nama BPKB dengan STNK<span style="color:hsla(0,100%,50%,1);">*</span>
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="ktp" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            Mengecek Nama STNK Dengan KTP Debitur/Pasangan<span style="color:hsla(0,100%,50%,1);">*</span>
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="motor" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            Mengecek No. Rangka di BPKB dengan Motor<span style="color:hsla(0,100%,50%,1);">*</span>
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="mesin_motor" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            Mengecek No. Mesin di BPKB dengan di Motor
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="asuransi" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                              D-079 Surat Pernyataan Asuransi jiwa
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="kwitansi" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                              Kwitansi
                            </label>
                          </div>
                        

                        <h2 style="text-align: center;">Dokumen Kondisional</h2>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="d-012" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            D-012 Jaminan Penanggungan Hutang (Personal Guarantee)
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="d-0128" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            D-0128 Jaminan Pembayaran (Payment Guarantee)
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="d-0188" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            D-0188 Surat Pernyataan & Persetujuan Pasangan
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="d-025" id="flexRadioDefault1">
                            <label class="form-check-label" value="1">
                            D-025 Surat Pernyataan Beda Data di indentitas
                            </label>
                          </div>
                                                                                                        
                        </div>
                        <div class="form-group mb-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>
