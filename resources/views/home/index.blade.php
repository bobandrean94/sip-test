<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-body">
                List Pengerjaan Service
                    <form action="" method="POST" enctype="multipart/form-data">
                        
                        <div class="card w-75">
                        <div class="card-body">
                            <h5 class="card-title">Foto Persyaratan</h5>
                            <a  href="{{ route('fotopersyaratan.index') }}" class="btn btn-primary">Input</a>
                        </div>
                        <div class="card w-75">
                        <div class="card-body">
                            <h5 class="card-title">Tanda Tangan Perjanjian</h5>
                            <a  href="{{ route('agreement.index') }}" class="btn btn-primary">Input</a>
                        </div>
                        </div>
                        <div class="card w-75">
                        <div class="card-body">
                            <h5 class="card-title">BPKB Kendaraan</h5>
                            <a href="{{ route('bpkb.index') }}" class="btn btn-primary">Input</a>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <h5 class="card-title">Get Header</h5>
            <a href="{{ route('serviceheader.index') }}" class="btn btn-primary">Click</a>
            <h5 class="card-title">Get Detail</h5>
            <a href="{{ route('servicedetail.index') }}" class="btn btn-primary">Click</a>
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>
