@if(session()->has('errors'))
    <ul class="alert alert-danger m-3" role="alert" style="list-style: none;">
        @foreach($errors->all() as $e)
            <li>{{ $e }}</li>
        @endforeach
    </ul>
@endif

@if(session()->has('success'))
    <div class="alert alert-success m-3" role="alert">
        {{ session('success') }}
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-danger m-3" role="alert">
        {{ session('error') }}
    </div>
@endif