<ul class="nav nav-pills nav-fill flex-column p-3">
    <li class="nav-item mb-2">
      <a class="nav-link active" aria-current="page" href="{{ route('blog.index') }}">Blog</a>
    </li>
    <li class="nav-item mb-2">
      <a class="nav-link active" href="{{ route('category.index') }}">Category</a>
    </li>
    <li class="nav-item mb-2">
      <a class="nav-link active" href="{{ route('tag.index') }}">Tags</a>
    </li>
</ul>