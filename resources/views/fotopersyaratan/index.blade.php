<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <h2 style="text-align: center;">Foto Persyaratan</h2>
                    <h5 style="text-align: center; color: red">* mandatory(Harus di lengkapi)</h5>

                    <form action="" method="POST" enctype="multipart/form-data">
                        
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 1</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 2</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 3</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 4</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 5</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 6</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 7</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                          <div class="form-group">
                            <label for="exampleFormControlFile1">Foto 8</label><br>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                          </div>
                        

                        <h2 style="text-align: center;">Dokumen Kondisional</h2>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                            D-012 Jaminan Penanggungan Hutang (Personal Guarantee)
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                            D-0128 Jaminan Pembayaran (Payment Guarantee)
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                            D-0188 Surat Pernyataan & Persetujuan Pasangan
                            </label>
                          </div>
                          <div class="form-check">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label" for="flexRadioDefault1">
                            D-025 Surat Pernyataan Beda Data di indentitas
                            </label>
                          </div>
                                                                                                        
                        </div>
                        <div class="form-group mb-2">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    </body>
</html>
