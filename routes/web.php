<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AgreementController;
use App\Http\Controllers\BpkbController;
use App\Http\Controllers\FotoPersyaratanController;
use App\Http\Controllers\ServiceHeaderController;
use App\Http\Controllers\ServiceDetailController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::group(['prefix' => 'perjanjian'], function () {
    Route::get('', [AgreementController::class, 'index'])->name('agreement.index');
    Route::post('store', [AgreementController::class, 'store'])->name('agreement.store');
});

Route::group(['prefix' => 'bpkb'], function () {
    Route::get('', [BpkbController::class, 'index'])->name('bpkb.index');
    Route::post('store', [BpkbController::class, 'store'])->name('bpkb.store');
});

Route::group(['prefix' => 'fotopersyaratan'], function () {
    Route::get('', [FotoPersyaratanController::class, 'index'])->name('fotopersyaratan.index');
    Route::post('store', [FotoPersyaratanController::class, 'store'])->name('fotopersyaratan.store');
});

Route::group(['prefix' => 'serviceheader'], function () {
    Route::get('', [ServiceHeaderController::class, 'index'])->name('serviceheader.index');
});

Route::group(['prefix' => 'servicedetail'], function () {
    Route::get('', [ServiceDetailController::class, 'index'])->name('servicedetail.index');
});
