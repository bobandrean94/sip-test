<?php

namespace App\Traits;

trait BaseRepositoryTraits
{
    public function all()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function getByIdWith($id, $relations)
    {
        return $this->model->where('id', $id)->with($relations)->first();
    }

    public function store($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data)
    {
        return $this->model->where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        return $this->model->where('id', $id)->delete();
    }
}
