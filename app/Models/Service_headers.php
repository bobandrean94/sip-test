<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tags extends Model
{
    use SoftDeletes;

    protected $table = 'service_headers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_name', 'mandatory', 'package_id', 'debitur_id', 'service_price_amount', 'service_discount_amount','status','created_by','updated_by','created_at','updated_at','deleted_at'
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
  
    public function Service_detail()
    {
        return $this->hasMany(Service_detail::class);
    }
}