<?php

namespace App\Repositories;

use App\Models\Post;
use App\Traits\BaseRepositoryTraits;

class PostRepository 
{
    use BaseRepositoryTraits;

    private $model;

    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    public function datatable()
    {
        return $this->model->with(['category', 'tags'])->paginate(10);
    }
}