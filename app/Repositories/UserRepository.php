<?php

namespace App\Repositories;

use App\Models\User;
use App\Traits\BaseRepositoryTraits;

class UserRepository 
{
    use BaseRepositoryTraits;

    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }
}