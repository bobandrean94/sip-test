<?php

namespace App\Repositories;

use App\Models\Tags;
use App\Traits\BaseRepositoryTraits;

class TagRepository 
{
    use BaseRepositoryTraits;

    private $model;

    public function __construct(Tags $model)
    {
        $this->model = $model;
    }
    public function datatable()
    {
        return $this->model->paginate(10);
    }
}