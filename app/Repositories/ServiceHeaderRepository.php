<?php

namespace App\Repositories;

use App\Models\ServiceHeader;
use App\Traits\BaseRepositoryTraits;

class ServiceHeaderRepository 
{
    use BaseRepositoryTraits;

    private $model;

    public function __construct(ServiceHeader $model)
    {
        $this->model = $model;
    }
    public function datatable()
    {
        return $this->model->paginate(10);
    }
}