<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ServiceHeaderRepository;
use Illuminate\Support\Facades\Auth;

class ServiceHeaderController extends Controller
{
    private $request;
    private $ServiceHeaderRepository;

    public function __construct(Request $request, Request $ServiceHeaderRepository)
    {
        $this->request = $request;
        $this->request = $ServiceHeaderRepository;
    }

    public function index()
    {
        $serviceHeader = $this->ServiceHeaderRepository->datatable();

        return $this->toJSON('Service Header List', $serviceHeader, 200);
    }

}
