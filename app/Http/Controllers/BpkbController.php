<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class BpkbController extends Controller
{
    private $request;
    private $categoryRepository;
    private $tagRepository;
    private $postRepository;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {

        return view('bpkb.index',);
    }

    public function store()
    {
        $this->validate($this->request,[
            'kondisi_bpkp' => 'required',
            'sinar_uv' => 'required',
            'stnk' => 'required',
            'ktp' => 'required',
            'motor' => 'required',
            'mesin_motor' => 'required',
            'asuransi' => 'required',
            'kwitansi' => 'required',
            'd-012' => 'required',
            'd-0128' => 'required',
            'd-025' => 'required',
            

        ]);

        $data['user_id'] = session()->get('user')->id;
        $data['kondisi_bpkp'] = $this->request->kondisi_bpkp;
        $data['sinar_uv'] = $this->request->sinar_uv;
        $data['stnk'] = $this->request->stnk;
        $data['ktp'] = $this->request->ktp;
        $data['motor'] = $this->request->motor;
        $data['mesin_motor'] = $this->request->mesin_motor;
        $data['asuransi'] = $this->request->asuransi;
        $data['kwitansi'] = $this->request->kwitansi;
        $data['d-012'] = $this->request->d-012;
        $data['d-0128'] = $this->request->D;
        $data['d-025'] = $this->request->d-025;
        
        $this->serviceDetailRepository->store($data);

        return redirect()->route('bpkb.index')->with('success', 'New post added.');
    }
  
    
    public function update($id){

        $this->validate($this->request,[
            'kondisi_bpkp' => 'required',
            'sinar_uv' => 'required',
            'stnk' => 'required',
            'ktp' => 'required',
            'motor' => 'required',
            'mesin_motor' => 'required',
            'asuransi' => 'required',
            'kwitansi' => 'required',
            'd-012' => 'required',
            'd-0128' => 'required',
            'd-025' => 'required',
            

        ]);

        $data['user_id'] = session()->get('user')->id;
        $data['kondisi_bpkp'] = $this->request->kondisi_bpkp;
        $data['sinar_uv'] = $this->request->sinar_uv;
        $data['stnk'] = $this->request->stnk;
        $data['ktp'] = $this->request->ktp;
        $data['motor'] = $this->request->motor;
        $data['mesin_motor'] = $this->request->mesin_motor;
        $data['asuransi'] = $this->request->asuransi;
        $data['kwitansi'] = $this->request->kwitansi;
        $data['d-012'] = $this->request->D-012;
        $data['d-0128'] = $this->request->D;
        $data['d-025'] = $this->request->D-025;
        
        $this->serviceDetailRepository->update($id,$data);

        return redirect()->route('bpkb.index')->with('success', ' bpkb Has been Updated.');
    }
    
    public function destroy($id){

       $this->serviceDetailRepository->destroy($id);

       return redirect()->route('bpkb.index')->with('error', ' bpkb Has been Deleted.');
    }
}
