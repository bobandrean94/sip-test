<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class AgreementController extends Controller
{
    private $request;
    private $categoryRepository;
    private $tagRepository;
    private $postRepository;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {

        return view('agreement.index',);
    }

    public function create()
    {
        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->all();

        return view('pages.agreement.create', compact('categories', 'tags'));
    }

    public function store()
    {
        $this->validate($this->request,[
            'D-0048-1' => 'required',
            'D-0048-2' => 'required',
            'D-025' => 'required',
            'F-024A' => 'required',
            'F-002' => 'required',
            'D-004B-1' => 'required',
            'D-079' => 'required',
            'kwitansi' => 'required',
            'D-012' => 'required',
            'D-0128' => 'required',
            'D-0188' => 'required',

        ]);

        $data['user_id'] = session()->get('user')->id;
        $data['D-0048-1'] = $this->request->D-0048-1;
        $data['D-0048-2'] = $this->request->D-0048-2;
        $data['D-025'] = $this->request->D-025;
        $data['F-024A'] = $this->request->F-024A;
        $data['F-002'] = $this->request->F-002;
        $data['D-004B-1'] = $this->request->D-004B-1;
        $data['D-079'] = $this->request->D-079;
        $data['kwitansi'] = $this->request->kwitansi;
        $data['D-012'] = $this->request->D-012;
        $data['D-0128'] = $this->request->D-0128;
        $data['D-0188'] = $this->request->D-0188;
        
        $this->serviceDetailRepository->store($data);

        return redirect()->route('bpkb.index')->with('success', 'New post added.');
    }
  
    
    public function update($id){

            $this->validate($this->request,[
                'D-0048-1' => 'required',
                'D-0048-2' => 'required',
                'D-025' => 'required',
                'F-024A' => 'required',
                'F-002' => 'required',
                'D-004B-1' => 'required',
                'D-079' => 'required',
                'kwitansi' => 'required',
                'D-012' => 'required',
                'D-0128' => 'required',
                'D-0188' => 'required',
    
            ]);
    
            $data['user_id'] = session()->get('user')->id;
            $data['D-0048-1'] = $this->request->D-0048-1;
            $data['D-0048-2'] = $this->request->D-0048-2;
            $data['D-025'] = $this->request->D-025;
            $data['F-024A'] = $this->request->F-024A;
            $data['F-002'] = $this->request->F-002;
            $data['D-004B-1'] = $this->request->D-004B-1;
            $data['D-079'] = $this->request->D-079;
            $data['kwitansi'] = $this->request->kwitansi;
            $data['D-012'] = $this->request->D-012;
            $data['D-0128'] = $this->request->D-0128;
            $data['D-0188'] = $this->request->D-0188;
        
        $this->serviceDetailRepository->update($id,$data);

        return redirect()->route('bpkb.index')->with('success', ' bpkb Has been Updated.');
    }
    
    public function destroy($id){

       $this->serviceDetailRepository->destroy($id);

       return redirect()->route('bpkb.index')->with('error', ' bpkb Has been Deleted.');
    }
}
