<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ServiceHeaderRepository;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    private $request;
    private $ServiceHeaderRepository;

    public function __construct(Request $request, Request $ServiceHeaderRepository)
    {
        $this->request = $request;
        $this->request = $ServiceHeaderRepository;
    }

    public function index()
    {

        return view('home.index');
    }

}
