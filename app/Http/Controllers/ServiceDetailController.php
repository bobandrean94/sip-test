<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ServiceHeaderRepository;
use App\Repositories\ServiceDetailRepository;
use Illuminate\Support\Facades\Auth;

class ServiceDetailController extends Controller
{
    private $request;
    private $ServiceHeaderRepository;
    private $serviceDetailRepository;


    public function __construct(Request $request, Request $ServiceHeaderRepository,Request $serviceDetailRepository)
    {
        $this->request = $request;
        $this->request = $ServiceHeaderRepository;
        $this->request = $serviceDetailRepository;
    }

    public function index()
    {
        $serviceDetail = $this->serviceDetailRepository->datatable();

    
        return $this->toJSON('Service Detail List', $serviceDetail, 200);

    }

}
